// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "mlhp/core.hpp"

// Simply solve     
// 
// udot = x * (1 - x)   on [0, 1]
//    u = 1/2           at x = 0
// 
// giving:
//   u  = x^2 * (1/2 + x/3) + 1/2

using namespace mlhp;

std::vector<double> solveLinearSystem( const linalg::UnsymmetricSparseMatrix& K, 
                                       const std::vector<double>& F )
{
    auto data = std::vector<double>( K.size1( ) * K.size2( ), 0.0 );
    auto p = std::vector<size_t>( K.size1( ) );
    auto u = std::vector<double>( K.size1( ) );

    for( size_t iRow = 0; iRow < K.size1( ); ++iRow )
    {
        for( auto iColumn = K.indptr( )[iRow]; iColumn < K.indptr( )[iRow + 1]; ++iColumn )
        {
            data[iRow * K.size2( ) + K.indices( )[iColumn]] = K.data( )[iColumn];
        }
    }

    linalg::lu( data.data( ), p.data( ), K.size1( ) );
    linalg::luSubstitute( data.data( ), p.data( ), K.size1( ), F.data( ), u.data( ) );

    return u;
}

bool petrov = false;

auto evaluateIntegrals( const BasisFunctionEvaluation<1>& shapes,
                        double weightDetJ, double* lhs, double* rhs )
{
    auto N = shapes.noalias( 0, 0 );
    auto dN = shapes.noalias( 0, 1 );
    auto test = petrov ? dN : N;

    auto ndof = shapes.ndof( );
    auto nblocks = shapes.nblocks( );
    auto x = shapes.xyz( )[0];

    linalg::unsymmetricElementLhs( lhs, ndof, nblocks, [&]( size_t i, size_t j )
    { 
        return test[i] * dN[j] * weightDetJ;
    } );

    linalg::elementRhs( rhs, ndof, nblocks, [&]( size_t i )
    { 
        return test[i] * std::sin( x * 9.0 * std::numbers::pi ) * weightDetJ;
    } );
}

auto print( const std::vector<double>& u )
{
    std::cout << "[" << u[0];
    for( size_t i = 1; i < u.size( ); ++i )
    {
        std::cout << ", " << u[i];
    }
    std::cout << "]," << std::endl;
}

auto compute( size_t n )
{
    auto grid = makeRefinedGrid<1>( { n }, { 1.0 } );
    auto basis = makeHpBasis<TensorSpace, 1>( grid, 1 );

    auto integrand = FunctorDomainIntegrand<1>( DiffOrders::FirstDerivatives, false, evaluateIntegrals );

    auto dirichlet = DofIndicesValuesPair { { 0 }, { 0.5 } };

    auto K = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, dirichlet.first );
    auto F = std::vector<double>( K.size1( ), 0.0 );

    integrateLinearSystemOnDomain( *basis, integrand, K, F, dirichlet );

    auto u = boundary::inflate( solveLinearSystem( K, F ), dirichlet );

    //print( u );

    auto uExact = []( std::array<double, 1> x ) { return (1.0 - std::cos( x[0] * 9.0 * std::numbers::pi ) ) / 9.0 / std::numbers::pi + 0.5; };
    auto duExact = []( std::array<double, 1> x ) { return std::sin( x[0] * 9.0 * std::numbers::pi ); };

    auto l2Integrand = L2ErrorIntegrand<1>( u, uExact );
    auto h1Integrand = EnergyErrorIntegrand<1>( u, { duExact } );

    auto l2 = integrateScalarFunctionsOnDomain( *basis, l2Integrand );
    auto h1 = integrateScalarFunctionsOnDomain( *basis, h1Integrand );

    return std::array { std::sqrt( l2[2] / l2[1] ), std::sqrt( h1[2] / h1[1] ) };
}

int main( )
{
    std::vector<double> l2N, h1N, l2dN, h1dN;

    for( size_t i = 1; i <= 10; ++i )
    {
        petrov = false;

        auto errors1 = compute( utilities::binaryPow<size_t>( i ) );

        l2N.push_back( errors1[0] );
        h1N.push_back( errors1[1] );

        petrov = true;

        auto errors2 = compute( utilities::binaryPow<size_t>( i ) );

        l2dN.push_back( errors2[0] );
        h1dN.push_back( errors2[1] );
    }

    std::cout << "l2 errors (bubnov, petrov):" << std::endl;
    print( l2N );
    print( l2dN );

    std::cout << "energy errors (bubnov, petrov):" << std::endl;
    print( h1N );
    print( h1dN );
}