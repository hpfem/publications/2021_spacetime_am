This project contains the examples for the publication __Space-time hp finite elements for heat evolution in laser-based additive manufacturing__. The preprint can be found [here](https://arxiv.org/pdf/2112.00155.pdf) and some of the result files are uploaded in the [Submission](https://gitlab.com/hpfem/publications/2021_spacetime_am/-/tags/Submission) tag of this repository. They can be visualized in Paraview.

## Space-time examples and oneMKL

To solve the linear systems in our space-time examples we use the Intel Pardiso sparse direct solver. Because of that we need an installation of the intel oneMKL to compile and run the space-time examples. You can install it as part of the oneAPI base kit which you can get for free. There should also be an option for downloading without creating an account.

Many times oneMKL is found automatically, but if not, then you can set the MKL_DIR to the directory `lib/cmake/mkl` within your oneAPI root.

If you are using Visual Studio you can set the `SETVARS_CONFIG` environment variable with a single whitespace which will cause VS to run the setvars.bat script automatically. Also, starting VS from the CMake GUI might not work (might not run setvars.bat).

