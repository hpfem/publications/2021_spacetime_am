// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"

namespace mlhp
{

struct Compute : public Units
{
    void compute( )
    {
        static constexpr size_t D = 2; // Set up specificly for D = 2

        // Laser model
        double laserSpeed = 800.0 * mm / s; 
        double laserD4Sigma = 170.0 * um;
        double laserPower = 179.2 / laserD4Sigma * W;
        double laserAbsorptivity = 0.33;
        double laserDepthScaling = 0.3;
        
        // Domain and initial condition
        auto lengths = std::array<double, D> { 0.6 * cm, 0.1 * cm };
        auto nelements = std::array<size_t, D> { 4, 1 };
        auto initialTemperature = 25.0 * C;

         // Laser source  
        CoordinateList<D> laserPositions = { { 0.0, lengths[1] }, { lengths[0], lengths[1] } };

        auto laserTrack = laser::makeTrack( laserPositions, laserSpeed, laserPower );
        auto source = laser::volumeSource<D>( laserTrack, laserD4Sigma, laserAbsorptivity, laserDepthScaling );
        auto duration = 4.0 / 3.0 * laserTrack.back( ).time;

        // Material
        double density = 8440.0 * kg / ( m * m * m ); 

        auto material = [=, this]( double T ) 
        { 
            double T0 = std::min( T, 1290.0 );
            double dT = T < 1290.0 ? 1.0 : 0.0;

            return NonlinearHeatParameters
            {
                .specificCapacity      = ( 2.47e-4 * T0 + 4.05e-1 ) * J  / ( g * C ) * density,
                .specificCapacityPrime = ( 2.47e-4 * dT           ) * J  / g         * density,
                .conductivity          = ( 1.50e-4 * T0 + 9.50e-2 ) * W / ( cm * C ),
                .conductivityPrime     = ( 1.50e-4 * dT           ) * W / cm
            }; 
        };

        auto phaseChange = PhaseChangeParameters
        {
            .density               = density,
            .latentHeat            = 2.8e5 * J / kg,
            .liquidConductivity    = 0.0, 
            .Ts                    = 1190.0 * C,
            .Tl                    = 1450.0 * C,
            .S                     = 1.0,
        };

        auto integrandFactory = phaseChangeIntegrandFactory<D>( material, phaseChange, source );

        // h- and p-refinement
        auto spatialDegrees = { 3, 3, 4, 4, 4, 4 };
        auto temporalDegrees = { 3, 3, 3, 3, 3, 3 };
        
        auto grading = perLevelGrading<D>( spatialDegrees, temporalDegrees );

        double zfactor = 0.7;

        // Refinement based on laser path (delay, sigma, level)
        std::vector refinements =
        {
            laser::Refinement { 0.00 * ms, 0.26 * mm, 4.0, zfactor },
            laser::Refinement { 0.50 * ms, 0.32 * mm, 3.5, zfactor },
            laser::Refinement { 1.20 * ms, 0.36 * mm, 2.5, zfactor }, // 0.40
            laser::Refinement { 6.00 * ms, 0.42 * mm, 1.5, zfactor }, // 0.50
        };

        auto slabDuration = 1.2 * ms;
        auto nslabs = static_cast<size_t>( std::ceil( duration / slabDuration ) );

        std::cout << "  simulation duration  : " << duration << "s" << std::endl;
        std::cout << "  laser duration       : " << laserTrack.back( ).time << "s" << std::endl;
        std::cout << "  laser path length    : " << laserTrack.back( ).time * laserSpeed << "cm" << std::endl;

         // Problem setup 
        auto baseGrid = CartesianGrid<D + 1>( array::insert( nelements, D, nslabs ),
                                              array::insert( lengths, D, duration ) );

        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        size_t slabSize = 1;
        bool useTrunkSpace = true;

        SlabState<D> slab( baseGrid, slabSize );

        for( slab.islab = 0; slab.islab < slab.nslabs( ); ++slab.islab )
        {
            std::cout << "Computing slab " << slab.islab + 1 << " / " << slab.nslabs( );

            // Create one grid including ghost slabs and then filter ghost cells
            auto unfilteredMesh = createWithGhostCells<D>( baseGrid, slab.cell0( ), slab.cell1( ) );

            unfilteredMesh->refine( refinementStrategy );

            auto mesh = filterGhostCells<D>( unfilteredMesh, slab.islab, slab.nslabs( ) );

            // Create basis on unfiltered grid and then remove ghost elements
            auto unfilteredBasis = makeSpaceTimeTrunkSpace<D>( unfilteredMesh, grading );

            auto basis = std::make_shared<ElementFilterBasis<D + 1>>( unfilteredBasis, mesh );

            // Compute initial slab dofs
            if( slab.islab == 0 )
            {
                auto temperatureFunction = spatial::constantFunction<D + 1>( initialTemperature );

                slab.dirichletDofs = boundary::boundaryDofs<D + 1>( temperatureFunction, *basis, { boundary::face( D, 0 ) } );
            }
            else
            {
                auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 0 ) } )[0];

                slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            }

            // Log number of dofs and solve system
            auto nboundary = slab.dirichletDofs.first.size( );
            auto ninternal = basis->ndof( ) - nboundary;

            std::cout << " (" << ninternal << " internal and " << nboundary << " boundary dofs)" << std::endl;

            auto dofs = solveNonlinearProblem<D>( *basis, slab.dirichletDofs, integrandFactory, { 2, 1 } );
            
            // Postprocess solution
            std::vector postprocessors = 
            { 
                makeSolutionPostprocessor<D + 1>( dofs, 1 ),
                makeFunctionPostprocessor( source, "Source" ),
                makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements )
            };

            // Postprocess 2D slices with 8 subcells per element and 4 slices in time per base cell
            postprocessSlabSlices<D>( array::make<size_t, D>( 8 ), postprocessors, *basis, 
               "outputs/spacetime_introduction_2D_slices", slab.cell0( ), slab.cell1( ), 4 );

            // Postprocess 3D volume with 8 subcells also in time (in Paraview scale time (z) with factor 100)
            postprocess( array::make<size_t, D + 1>( 8 ), postprocessors, *basis, 
                "outputs/spacetime_introduction_2D_volume_" + std::to_string( slab.islab ), 1 );

            // Extract dofs for next slab
            auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 1 ) } )[0];

            slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            slab.dirichletDofs.second = algorithm::extract( dofs, slab.dirichletDofs.first );

        } // for islab
    } // Compute::compute
};

} // namespace mlhp

int main( )
{
    mlhp::Compute { }.compute( );
}

