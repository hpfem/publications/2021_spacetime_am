// This file is part of the 2021_spacetime_am project. License: See LICENSE

// Stuff related to the space-time discretization
// * Handling of ghost elements before and after time slabs
// * Wrapper for Intel Pardiso solver (for time-stepping we use an iterative solver)
// * Nonlinear solution iterations for thermal problem
// * Postprocessing 3D slices for a 4D finite element solution

#ifndef SPACETIME_AM_HELPER_SPACETIME_HPP
#define SPACETIME_AM_HELPER_SPACETIME_HPP

#include "mlhp/core.hpp"
#include "mlhp/core/postprocessing_impl.hpp"

#include "vtu11/vtu11.hpp"

#include <mkl_pardiso.h>

namespace mlhp
{

template<size_t D>
class SlabState
{
public:
    SlabState( CartesianGrid<D + 1> grid,
               size_t maxSlabSize ) :
        baseGrid( grid ), islab { 0 }
    {
        auto ncells = grid.ncells( D );
        auto nslabs = ( ncells - 1 ) / maxSlabSize + 1;

        partitioning = utilities::divideIntoChunks( ncells, nslabs );
    }

    size_t islab;

    // Number of slabs
    size_t nslabs( ) const { return partitioning[0]; }

    // index in time of first (non-ghost) base cell in slab
    size_t cell0( ) const { return utilities::chunkRange( islab, partitioning )[0]; }

    // index in time of one past last (non-ghost) base cell in slab
    size_t cell1( ) const { return utilities::chunkRange( islab, partitioning )[1]; }

    // number of (non-ghost) base cells in time for current slab
    size_t ncells( ) const { return cell1( ) - cell0( ); }

    // Time bounds of active slab (excluding ghost cells)
    double t0( ) const { return baseGrid.coordinates( ).back( )[cell0( )]; }
    double t1( ) const { return baseGrid.coordinates( ).back( )[cell1( )]; }

    // Base grid for all (!) slabs
    CartesianGrid<D + 1> baseGrid;

    DofIndicesValuesPair dirichletDofs;

private:
    std::array<size_t, 3> partitioning;
};

template<size_t D>
auto createWithGhostCells( const CartesianGrid<D + 1>& grid, size_t begin, size_t end )
{
    MLHP_CHECK( end < grid.ncells( D ) + 1, "Cell index out of bounds." );
    MLHP_CHECK( begin < end, "Invalid time cell index." );

    auto coordinates = grid.coordinates( );
    auto& slices = coordinates.back( );

    auto offset0 = begin - std::min( begin, size_t { 1 } );
    auto offset1 = end + 1 + ( end != grid.ncells( D ) ? 1 : 0 );

    slices.erase( slices.begin( ) + utilities::ptrdiff( offset1 ), slices.end( ) );
    slices.erase( slices.begin( ), slices.begin( ) + utilities::ptrdiff( offset0 ) );

    return makeRefinedGrid( CartesianGrid<D + 1> { coordinates } );
}
                
namespace detail
{

template<size_t D>
using BoundingBox = std::array<std::array<double, D>, 2>;

bool insideInterval( double coordinate, double x1, double x2 )
{
    return coordinate >= ( x1 - 1e-12 ) && coordinate <= ( x2 + 1e-12 );
}

// Get all leaf cells that are entirely inside bounding box
template<size_t D>
std::vector<CellIndex> elementsInBoundingBox( const AbsHierarchicalGrid<D>& grid,
                                              std::array<double, D> boundingBoxX1,
                                              std::array<double, D> boundingBoxX2 )
{
    std::vector<CellIndex> result;

    auto mapping = grid.createMapping( );

    for( CellIndex iLeaf = 0; iLeaf < grid.nleaves( ); ++iLeaf )
    {
        grid.prepareMapping( iLeaf, *mapping );

        auto cellX1 = mapping->map( array::make<double, D>( -1.0 ) );
        auto cellX2 = mapping->map( array::make<double, D>( +1.0 ) );

        bool inside = true;

        // Intersect intervals in each direction
        for( size_t axis = 0; axis < D; ++axis )
        {
            inside = inside && insideInterval( cellX1[axis], boundingBoxX1[axis], boundingBoxX2[axis] )
                            && insideInterval( cellX2[axis], boundingBoxX1[axis], boundingBoxX2[axis] );
        }

        if( inside )
        {
            result.push_back( iLeaf );
        }
    }

    return result;
}

} // namespace detail

template<size_t D>
std::vector<bool> maskGhostElements( const AbsHierarchicalGrid<D>& grid,
                                     bool deactivateFirst,
                                     bool deactivateLast )
{
    auto baseGridPtr = dynamic_cast<const CartesianGrid<D>*>( &grid.baseGrid( ) );

    MLHP_CHECK( baseGridPtr != nullptr, "Type is not CartesianGrid." );

    const auto& baseGrid = *baseGridPtr;

    auto gridBoundingBox = baseGrid.boundingBox( );

    std::vector<bool> mask( grid.nleaves( ), true );

    if( deactivateFirst )
    {
        auto boundingBox = gridBoundingBox;

        boundingBox[1].back( ) = mesh::map( baseGrid, 0, array::make<double, D>( 1.0 ) ).back( );

        auto indices = detail::elementsInBoundingBox( grid, boundingBox[0], boundingBox[1] );

        for( auto index : indices )
        {
            mask[index] = false;
        }
    }

    if( deactivateLast )
    {
        auto boundingBox = gridBoundingBox;

        boundingBox[0].back( ) = mesh::map( baseGrid, baseGrid.ncells( ) - 1, array::make<double, D>( -1.0 ) ).back( );

        auto indices = detail::elementsInBoundingBox( grid, boundingBox[0], boundingBox[1] );

        for( auto index : indices )
        {
            mask[index] = false;
        }
    }

    return mask;
}

template<size_t D>
auto filterGhostCells( const HierarchicalGridSharedPtr<D + 1>& unfilteredGrid,
                       size_t islab, size_t nslabs )
{
    auto ghostLeafMask = maskGhostElements( *unfilteredGrid, islab != 0, islab + 1 != nslabs );

    return std::make_shared<FilteredMesh<D + 1>>( unfilteredGrid, ghostLeafMask );
}

template<size_t D>
auto makeSpaceTimeTensorSpace( const HierarchicalGridSharedPtr<D + 1>& extendedGrid,
                               const PolynomialDegreeDistributor<D + 1>& distributor )
{
    return makeHpBasis<TensorSpace>( extendedGrid, distributor );
}

// Spatial trunk space with tensor product in time
template<size_t D>
auto makeSpaceTimeTrunkSpace( const HierarchicalGridSharedPtr<D + 1>& extendedGrid, 
                              const PolynomialDegreeDistributor<D + 1>& distributor )
{
    std::function maskInitializer = []( BooleanMask<D + 1>& mask, std::array<size_t, D + 1> degrees )
    { 
        auto shape = array::add<size_t>( degrees, 1 );
        auto maxShape = *std::max_element( shape.begin( ), shape.end( ) - 1 );

        mask.resize( shape );

        // Activate when i + j + ... < max( p_0, p_1, ... )
        nd::executeWithIndex( shape, [&]( std::array<size_t, D + 1> ijk, size_t index )
        { 
            mask[index] = std::accumulate( ijk.begin( ), ijk.end( ) - 1, size_t { 0 } ) < maxShape;
        } );

        // For each direction copy 0-slice in axis to 1-slice
        for( size_t axis = 0; axis < D; ++axis )
        {
            nd::execute( array::setEntry<size_t>( shape, axis, 1 ), [&]( std::array<size_t, D + 1> ijk )
            { 
                mask[array::setEntry<size_t>( ijk, axis, 1 )] = mask[ijk];
            } );
        }
    };

    return std::make_shared<MultilevelHpBasis<D + 1>>( extendedGrid,
        distributor( *extendedGrid, 1 ), maskInitializer );
}

auto pardisoSolve( const linalg::UnsymmetricSparseMatrix& matrix,
                   const std::vector<double>& rhs )
{
    void* internalSolverMemoryPointer[64];

    MKL_INT iparm[64];
    MKL_INT maximuNumberOfFactorization = 1;
    MKL_INT matrixNumber = 1;
    MKL_INT matrixType = 11;
    MKL_INT numberOfRightHandSides = 1;
    MKL_INT n = static_cast<MKL_INT>( rhs.size( ) );
    MKL_INT error = 0;
    MKL_INT integerDummy = 0;
    MKL_INT messageLevel = 0;

    std::fill( internalSolverMemoryPointer, internalSolverMemoryPointer + 64, nullptr );
    std::fill( iparm, iparm + 64, 0 );

    PARDISOINIT( internalSolverMemoryPointer, &matrixType, iparm );

    iparm[1] = 3;  // fill in reducing ordering: 3-> parallel version of metis
    //iparm[9] = 8; // Pivoting perturbation : 10 ^ -8
    iparm[23] = 1; // Parallel factorization control: 1->improved two-level factorization algorithm
    iparm[26] = 1; // Enable matrix consistency checks
    iparm[34] = 1; // One- or zero-based indexing of columns and rows: 1->zero based

    enum class PardisoPhase : MKL_INT
    {
        REORDER = 11, FACTORIZATION = 22, SOLUTION = 33, CLEANUP = -1
    };
    
    auto callPardiso = [&]( PardisoPhase phase, double* data, MKL_INT* indices, 
                            MKL_INT* indptr, double* right, double* solution )
    {
        MKL_INT phaseInt = static_cast<MKL_INT>( phase );

        PARDISO_64( internalSolverMemoryPointer, &maximuNumberOfFactorization, &matrixNumber, 
                    &matrixType, &phaseInt, &n, data, indptr, indices, &integerDummy, 
                    &numberOfRightHandSides, iparm, &messageLevel, right, solution, &error );

        if( error != 0 ) std::cout << "Error in pardiso solver." << std::endl;
    };

    MLHP_CHECK( -1 == ~0, "Reinterpret cast to signed requires two's complement." );
    MLHP_CHECK( sizeof( linalg::SparsePtr ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );
    MLHP_CHECK( sizeof( linalg::SparseIndex ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );

    // Reinterpret cast to signed works if machine uses two's complement (as tested above).
    auto* i = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indices( ) ) );
    auto* j = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indptr( ) ) );
    auto* v = const_cast<double*>( matrix.data( ) );
    auto* f = const_cast<double*>( rhs.data( ) );

    std::vector<double> solution( rhs.size( ) );

    callPardiso( PardisoPhase::REORDER, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::FACTORIZATION, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::SOLUTION, v, i, j, f, solution.data( ) );
    callPardiso( PardisoPhase::CLEANUP, nullptr, nullptr, nullptr, nullptr, nullptr );

    return solution;
}

// Same integration order everywhere (p + spatialOffset in space, p + temporalOffset in time)
auto uniformIntegrationOrderOffset( size_t spatialOffset, size_t temporalOffset )
{
    return [=]( CellIndex ielement, auto orders )
    {
        for( size_t axis = 0; axis + 1 < orders.size( ); ++axis )
        {
            orders[axis] += spatialOffset;
        }

        orders.back( ) += temporalOffset;
        
        return orders;
    };
}

template<size_t D>
using IntegrandFactory = std::function<DomainIntegrand<D + 1>(const AbsBasis<D + 1>&, 
    const std::vector<double>& dofs, size_t iteration, bool jacobian )>;

template<size_t D>
using QuadratureDeterminor = std::function<QuadratureOrderDeterminor<D + 1>( 
    const std::vector<double>& dofs, size_t iteration )>;

template<size_t D>
auto makeAssembly( const ElementFilterBasis<D + 1>& basis,
                   const DofIndicesValuesPair& dirichletDofs,
                   const IntegrandFactory<D>& createIntegrand,
                   const std::array<size_t, 2>& quadratureOrderOffsets )
{
    auto zeroDirichlet = std::make_pair( dirichletDofs.first, 
        std::vector<double>( dirichletDofs.first.size( ), 0.0 ) );

    return [&, zeroDirichlet]( linalg::UnsymmetricSparseMatrix* jacobian, 
                               const std::vector<double>& x, 
                               std::vector<double>& f, 
                               size_t iteration )
    {
        std::fill( f.begin( ), f.end( ), 0.0 );

        if( jacobian ) std::fill( jacobian->data( ), jacobian->data( ) + jacobian->nnz( ), 0.0 );

        auto allDofs = boundary::inflate( x, dirichletDofs );
        auto integrand = createIntegrand( basis, allDofs, iteration, jacobian );
        auto accuracy = uniformIntegrationOrderOffset( quadratureOrderOffsets[0], quadratureOrderOffsets[1] );

        auto targets = jacobian ? AssemblyTargetVector { *jacobian, f } : AssemblyTargetVector { f };

        integrateOnDomain<D + 1>( basis, integrand, targets, accuracy, zeroDirichlet );

        return std::sqrt( std::inner_product( f.begin( ), f.end( ), f.begin( ), 0.0 ) );
    };
}

// Standard Newton iterations without step size control
template<size_t D>
auto solveNonlinearProblem( const ElementFilterBasis<D + 1>& basis,
                            const DofIndicesValuesPair& dirichletDofs_, 
                            const IntegrandFactory<D>& createIntegrand,
                            std::array<size_t, 2> quadratureOffset = { 1, 0 } )
{
    auto dirichletDofs = boundary::makeUnique(dirichletDofs_);
    auto assemble = makeAssembly<D>( basis, dirichletDofs, createIntegrand, quadratureOffset );

    auto df = allocateMatrix<linalg::UnsymmetricSparseMatrix>( basis, dirichletDofs.first );
    auto x = std::vector<double>( df.size1( ), 0.0 );
    auto f = std::vector<double>( df.size1( ), 0.0 );

    double firstNorm = 1.0;

    for( size_t iteration = 0; iteration < 20; ++iteration )
    {
        std::cout << "    residual " << iteration + 1 << ": ";

        // Assemble
        double norm0 = assemble( &df, x, f, iteration );

        firstNorm = iteration != 0 ? firstNorm : norm0;

        std::cout << norm0 / firstNorm << " (" << norm0 << ")" << std::endl;

        // Solve
        auto dx = pardisoSolve( df, f );

        // Check convergence
        std::transform( x.begin( ), x.end( ), dx.begin( ), x.begin( ), std::minus<double> { } );

        auto norm1 = assemble( nullptr, x, f, iteration );

        if( norm1 / firstNorm <= 8e-5 && iteration >= 2 )
        {
            std::cout << "    final residual: " << norm1 / firstNorm << " (" << norm1 << ")" << std::endl;

            return boundary::inflate( x, dirichletDofs );
        }
    }

    MLHP_THROW( "Nonlinear iterations did not converge." );
}

// Determine step size for increment using a bisection algorithm
template<size_t D>
auto solveNonlinearProblemBisection( const ElementFilterBasis<D + 1>& basis,
                                     const DofIndicesValuesPair& dirichletDofs, 
                                     const IntegrandFactory<D>& createIntegrand,
                                     std::array<size_t, 2> quadratureOffset = { 1, 0 },
                                     size_t numberOfEvaluations = 8 )
{
    auto assemble = makeAssembly<D>( basis, dirichletDofs, createIntegrand, quadratureOffset );

    auto df = allocateMatrix<linalg::UnsymmetricSparseMatrix>( basis, dirichletDofs.first );
    auto x = std::vector<double>( df.size1( ), 0.0 );
    auto f = std::vector<double>( df.size1( ), 0.0 );
    auto xb = std::vector<double>( df.size1( ), 0.0 );

    double firstNorm = 1.0;

    for( size_t iteration = 0; iteration < 20; ++iteration )
    {
        std::cout << "    residual " << iteration + 1 << ": ";

        double norm0 = assemble( &df, x, f, iteration );

        firstNorm = iteration != 0 ? firstNorm : norm0;

        // Solve
        auto dx = pardisoSolve( linalg::filterZeros( df ), f );

        // Determine optimal step
        auto evaluateResidual = [&]( double beta )
        { 
            std::transform( x.begin( ), x.end( ), dx.begin( ), xb.begin( ), 
                [&]( double x_, double dx_ ){ return x_ - beta * dx_; } );

            return assemble( nullptr, xb, f, iteration );
        };

        auto [beta, norm1] = lineSearch( evaluateResidual, numberOfEvaluations, norm0 );

        // Check convergence
        std::transform( x.begin( ), x.end( ), dx.begin( ), x.begin( ), 
            [&]( double x_, double dx_ ){ return x_ - beta * dx_; } );

        std::cout << norm1 / firstNorm << " (" << norm1 << "), beta = " << beta << std::endl;

        if( norm1 / firstNorm <= 1e-6 && iteration >= 2 )
        {
            return boundary::inflate( x, dirichletDofs );
        }
    }

    MLHP_THROW( "Nonlinear iterations did not converge." );
}

namespace detail
{

// Vector of [tmin, tmax] for each cell
using TimeBoundsOfCells = std::vector<std::array<double, 2>>;

// A slab is sliced in time. This type contains cell indices and local time coordinates for one slice
using SlicedCellsForTick = std::vector<std::tuple<CellIndex, double>>;

// Compute [tmin, tmax] for each cell in basis
template<size_t D>
auto computeTimeBounds( const ElementFilterBasis<D>& basis )
{
    TimeBoundsOfCells cellBoundsInTime( basis.nelements( ) );

    auto& mesh = basis.mesh( );
    auto mapping = mesh.createMapping( );

    for( CellIndex iCell = 0; iCell < basis.nelements( ); ++iCell )
    {
        mesh.prepareMapping( iCell, *mapping );

        auto rst1 = array::setEntry( array::make<double, D>( 0.0 ), D - 1, -1.0 );
        auto rst2 = array::setEntry( array::make<double, D>( 0.0 ), D - 1, +1.0 );

        cellBoundsInTime[iCell] = { mapping->map( rst1 )[D - 1], mapping->map( rst2 )[D - 1] };
    }

    return cellBoundsInTime;
}

auto computeSlabBounds( const TimeBoundsOfCells& cellBounds )
{
    std::array minmax { cellBounds.front( )[0], cellBounds.front( )[1] };

    for( size_t iCell = 1; iCell < cellBounds.size( ); ++iCell )
    {
        minmax[0] = std::min( minmax[0], cellBounds[iCell][0] );
        minmax[1] = std::max( minmax[1], cellBounds[iCell][1] );
    }

    return minmax;
}

// Find minimum and maximun time values for slab and partition linearly
auto linearSlabSliceTicks( std::array<double, 2> slabBounds,
                           size_t numberOfSlices,
                           bool removeFirst )
{
    auto ticks = utilities::linspace( slabBounds[0], slabBounds[1], numberOfSlices + 1 );

    if( removeFirst )
    {
        ticks.erase( ticks.begin( ) );
    }

    return ticks;
}

template<size_t D>
auto sliceSlabCellsInTime( const ElementFilterBasis<D>& basis,
                           size_t nslices, size_t icell )
{
    auto tBounds = computeTimeBounds( basis );
    auto slabBounds = computeSlabBounds( tBounds );
    auto slabTicks = linearSlabSliceTicks( slabBounds, nslices, icell != 0 );

    std::vector<SlicedCellsForTick> slices( slabTicks.size( ) );

    for( CellIndex iCell = 0; iCell < basis.nelements( ); ++iCell )
    {
        auto [tmin, tmax] = tBounds[iCell];

        for( size_t iTick = 0; iTick < slabTicks.size( ); ++iTick )
        {
            double local = utilities::mapToLocal1( tmin, tmax, slabTicks[iTick] );

            double eps = 1e-12;

            // If tick is in [cell min, cell max) or cell max is slab max
            if( local > -1.0 - eps && ( local < 1.0 - eps || tmax > slabBounds[1] - eps ) )
            {
                slices[iTick].emplace_back( iCell, local );
            }
        }
    }

    return slices;
}

} // namespace detail

template<size_t D>
void postprocessSlabSlices( std::array<size_t, D> nsamples,
                            const ElementPostprocessors<D + 1>& postprocessors,
                            const ElementFilterBasis<D + 1>& basis,
                            const std::string& fileBaseName,
                            size_t cellBegin, size_t cellEnd, size_t slicesPerCell,
                            PostprocessTopologies topologies = defaultOutputTopologies[D] )
{
    auto nslices = slicesPerCell * ( cellEnd - cellBegin );
    auto slices = detail::sliceSlabCellsInTime( basis, nslices, cellBegin );

    #pragma omp parallel for schedule( dynamic )
    for( int ii = 0; ii < static_cast<int>( slices.size( ) ); ++ii )
    {
        size_t iSlice = static_cast<size_t>( ii );

        std::vector<double> points;
        std::vector<vtu11::VtkIndexType> connectivity, offsets;
        std::vector<vtu11::VtkCellType> types;

        CoordinateGrid<D> rstVectors1;
        CoordinateGrid<D + 1> rstVectors2;
        CoordinateList<D> xyzList1;
        CoordinateList<D + 1> xyzList2;

        vtu11::Vtu11UnstructuredMesh mesh { points, connectivity, offsets, types };

        auto dataSetInfo = mlhp::detail::extractDataSetInfo<D + 1>( postprocessors );

        mlhp::detail::PostprocessorEvaluator<D + 1> evaluator( basis, postprocessors, dataSetInfo );

        for( auto [iElement, t] : slices[iSlice] )
        {
            mlhp::detail::createPostprocessingGrid<D>( rstVectors1, nsamples );

            std::copy( rstVectors1.begin( ), rstVectors1.end( ), rstVectors2.begin( ) );

            rstVectors2.back( ) = { t };

            evaluator.evaluatePostprocessors( iElement, rstVectors2, xyzList2 );

            xyzList1.resize( xyzList2.size( ) );

            std::transform( xyzList2.begin( ), xyzList2.end( ), xyzList1.begin( ),
                            []( auto xyz ) { return array::slice( xyz, D ); } );

            mlhp::detail::appendVtuPostprocessingGrid( rstVectors1,
                xyzList1, topologies, points, connectivity, offsets, types );
        }

        size_t fileIndex = iSlice + ( cellBegin != 0 ? cellBegin * slicesPerCell + 1 : 0 );

        std::string filename = fileBaseName + "_" + std::to_string( fileIndex ) + ".vtu";

        std::filesystem::create_directories( std::filesystem::path( filename ).remove_filename( ) );
        vtu11::writeVtu( filename, mesh, dataSetInfo, evaluator.data( ) );
    }
}

template<size_t D>
ElementPostprocessor<D> makeDerivativePostprocessor( const std::vector<double>& solution,
                                                            size_t direction, std::string name )
{
    auto function = [=]( const BasisFunctionEvaluation<D>& shapes,
                         const DofIndexVector& locationMap,
                         std::span<double> target )
    {
        auto N = shapes.noalias( 0, 1 ) + direction * shapes.ndofpadded( );
            
        for( size_t idof = 0; idof < shapes.ndof( ); ++idof )
        {
            target[0] += solution[locationMap[idof]] * N[idof];
        }
    };

    return ElementPostprocessor<D>( function, 1, 1, name );
}

} // namespace mlhp

#endif // SPACETIME_AM_HELPER_SPACETIME_HPP
