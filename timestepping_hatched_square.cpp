// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_am.hpp"

namespace mlhp
{

struct Compute : public Units
{   
    void compute( )
    {
        static constexpr size_t D = 3;

        // Laser model
        double laserSpeed = 800.0 * mm / s;
        double laserD4Sigma = 170.0 * um;
        double laserPower = 179.2 * W;
        double laserAbsorptivity = 0.32;
        double laserDepthScaling = 0.28;

        // Domain and initial condition
        auto lengths = std::array<double, D> { 24.08 * mm, 24.82 * mm, 3.18 * mm };
        auto nelements = std::array<size_t, D> { 12, 12, 3 };
        auto initialTemperature = 25.0 * C;

        // Laser source 
        auto trackDistance = 100.0 * um;
        auto squareWidth = 10.0 * mm;
        auto squareCenter = std::array { lengths[0] / 2.0, lengths[1] / 2.0, lengths[2] };

        auto laserPositions = laser::hatchedSquarePositions( squareCenter, squareWidth, trackDistance );

        auto laserTrack = laser::makeTrack( laserPositions, laserSpeed, laserPower );
        auto source = laser::volumeSource<D>( laserTrack, laserD4Sigma, laserAbsorptivity, laserDepthScaling );
        auto duration = 3000.0 * ms; // laserTrack.back( ).time;

        //printTrack( laserTrack );
        
        // Material
        double density = 8440.0 * kg / ( m * m * m );

        auto material = [=, this]( double T ) 
        { 
            double T0 = std::min( T, 1290.0 );
            double dT = T < 1290.0 ? 1.0 : 0.0;

            return NonlinearHeatParameters
            {
                .specificCapacity      = ( 2.47e-4 * T0 + 4.05e-1 ) * J  / ( g * C ) * density,
                .specificCapacityPrime = ( 2.47e-4 * dT           ) * J  / g         * density,
                .conductivity          = ( 1.50e-4 * T0 + 9.50e-2 ) * W / ( cm * C ),
                .conductivityPrime     = ( 1.50e-4 * dT           ) * W / cm
            }; 
        };

        auto phaseChange = PhaseChangeParameters
        {
            .density               = density,
            .latentHeat            = 2.8e5 * J / kg,
            .liquidConductivity    = 0.0, 
            .Ts                    = 1290.0 * C,
            .Tl                    = 1550.0 * C,
            .S                     = 1.0,
        };
 
        // Discretization
        using AnsatzSpace = TrunkSpace;

        auto degrees = std::vector<PolynomialDegree> { 2, 2, 4, 4, 3, 3 };

        auto grading = PerLevelGrading { degrees };

        auto nsteps = static_cast<size_t>( std::ceil( duration / ( 0.075 * ms ) ) );

        auto dt = duration / nsteps;
        double theta = 0.5;

        // Refinement based on laser path (delay, sigma, level, zfactor)
        std::vector refinements =
        {
            laser::Refinement { 0.0 * ms, 0.18 * mm, 5.4, 0.5 },
            laser::Refinement { 1.2 * ms, 0.24 * mm, 3.5, 0.5 },
            laser::Refinement { 6.0 * ms, 0.40 * mm, 2.5, 0.8 },
            laser::Refinement { 30.0 * ms, 0.9 * mm, 1.5, 1.0 },
            laser::Refinement { 100.0 * ms, 1.1 * mm, 1.0, 1.0 },
        };

        auto initialCondition = spatial::constantFunction<D>( initialTemperature );

        std::cout << "  simulation duration  : " << duration << "s" << std::endl;
        std::cout << "  laser duration       : " << laserTrack.back( ).time << "s" << std::endl;
        std::cout << "  laser path length    : " << laserTrack.back( ).time * laserSpeed << "cm" << std::endl;

        // Initial discretization
        auto grid0 = makeRefinedGrid( nelements, lengths );
        auto grid1 = grid0;

        auto basis0 = makeHpBasis<AnsatzSpace>( grid0, grading );
        auto dofs0 = projectOnto( *basis0, initialCondition );

        // Postprocess initial condition
        std::vector postprocessors =
        {
            makeSolutionPostprocessor<D>( dofs0, 1 ),
            //makeFunctionPostprocessor<D>( spatial::sliceLast( source, 0.0 ), "Source" ),
            //makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, 0.0 )
        };

        auto resolution = array::make<size_t, D>( 4 );
        auto filename = "outputs/timestepping_hatched_square_";

        postprocess( resolution, postprocessors, *basis0, filename + std::to_string( 0 ), 1 );

        size_t vtuInterval = 8;

        // Assembly stuff
        auto partitioner = NoPartitioner<D> { };
        auto solve = linalg::makeCGSolver( );
 
        // Time stepping
        for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
        {
            double time0 = iTimeStep * dt;
            double time1 = ( iTimeStep + 1 ) * dt;

            // Create discetisation for t^{i+1}
            grid1 = makeRefinedGrid( nelements, lengths );
            grid1->refine( laser::makeRefinement<D>( laserTrack, refinements, time1 ) );
            
            auto basis1 = makeHpBasis<AnsatzSpace>( grid1, grading );

            std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
            std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;

            auto df = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis1 );

            std::vector<double> dofs1 = projectOnto( *basis0, *basis1, dofs0, df );
            std::vector<double> f( df.size1( ), 0.0 );

            double norm0 = 0.0;

            // Newton-Raphson iterations
            for( size_t i = 0; i < 40; ++i )
            {
                std::fill( f.begin( ), f.end( ), 0.0 );
                std::fill( df.data( ), df.data( ) + df.nnz( ), 0.0 );

                auto integrand = makeTimeSteppingThermalIntegrand<D>( material, phaseChange,
                    source, dofs0, dofs1, std::array{ time0, time1 }, theta );

                //auto quadrature = overintegratePhaseChange<D>( *basis1, dofs1, phaseChange, i, 3 );
                auto quadrature = makeIntegrationOrderDeterminor<D>( 2 ); // p + 2

                integrateOnDomain<D>( *basis0, *basis1, integrand, { df, f }, partitioner, quadrature );

                double norm1 = std::sqrt( std::inner_product( f.begin( ), f.end( ), f.begin( ), 0.0 ) );

                norm0 = i == 0 ? norm1 : norm0;

                std::cout << "iteration " << i + 1 << ": | F | = " << norm1 / norm0 << " (" << norm1 << ")" << std::endl;

                auto dx = solve( df, f );

                std::transform( dofs1.begin( ), dofs1.end( ), dx.begin( ), dofs1.begin( ), std::minus<double> { } );

                if(  norm1 / norm0 <= 1e-4 ) break;
            }

            // Postprocess vtu file with 6 sub-cells per element
            std::vector postprocessors =
            {
                makeSolutionPostprocessor<D>( dofs1, 1 ),
                //makeFunctionPostprocessor<D>( spatial::sliceLast( source, time1 ), "Source" ),
                //makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, time1 )
            };

            if( ( iTimeStep + 1 ) % vtuInterval == 0 )
            {
                postprocess( resolution, postprocessors, *basis1, filename +
                    std::to_string( ( iTimeStep / vtuInterval ) + 1 ), 1 );
            }

            // Move discretization for next step
            dofs0 = std::move( dofs1 );
            grid0 = std::move( grid1 );
            basis0 = std::move( basis1 );
        }
    }
};

} // namespace mlhp

int main( )
{
    mlhp::Compute { }.compute( );
}


