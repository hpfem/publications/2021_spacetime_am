// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_am.hpp"

namespace mlhp
{

struct Compute : public Units
{   
    void compute( )
    {
        static constexpr size_t D = 3;

        // Domain, base mesh and initial condition
        auto lengths = std::array<double, D> { 1000 * um, 600 * um, 300 * um };
        auto nelements = std::array<size_t, D> { 10 * 2, 6 * 2, 3 * 2 };
        auto initialTemperature = 25.0 * C;
       
        // Laser model
        double laserSpeed = 800.0 * mm / s;
        double laserD4Sigma = 170.0 * um;
        double laserPower = 179.2 * W;
        double laserAbsorptivity = 0.37;
        double laserDepthScaling = 0.3;
 
        // Discretization
        using AnsatzSpace = TrunkSpace;

        auto degrees = std::vector<PolynomialDegree> { 3, 3, 3 };

        auto grading = PerLevelGrading { degrees };

        size_t nsteps = 64;
        double theta = 0.5;

        // Laser source 
        CoordinateList<D> laserPositions
        {
            {  0 * um,  lengths[1] / 2.0, lengths[2] },
            {  880 * um,  lengths[1] / 2.0, lengths[2] }
        };

        auto laserTrack = laser::makeTrack( laserPositions, laserSpeed, laserPower );
        auto volumeSource = laser::volumeSource<D>( laserTrack, laserD4Sigma, laserAbsorptivity, laserDepthScaling );
        auto duration = laserTrack.back( ).time;

        // Material
        double density = 8440.0 * kg / ( m * m * m );

        auto material = [=, this]( double T0 )
        {
            double T = std::min( T0, 1290.0 );
            double dT = T0 < 1290.0 ? 1.0 : 0.0;

            return NonlinearHeatParameters
            {
                .specificCapacity      = ( 2.47e-4 * T + 4.05e-1 ) * J / ( g * C ) * density,
                .specificCapacityPrime = ( 2.47e-4 * dT          ) * J / g         * density,
                .conductivity          = ( 1.50e-4 * T + 9.50e-2 ) * W / ( cm * C ),
                .conductivityPrime     = ( 1.50e-4 * dT          ) * W / cm
            };
        };

        auto phaseChange = PhaseChangeParameters
        {
            .density               = density,
            .latentHeat            = 2.8e5 * J / kg,
            .liquidConductivity    = 0.0,
            .Ts                    = 1290.0 * C,
            .Tl                    = 1350.0 * C,
            .S                     = 1.0
        };

        // Refinement based on laser path (delay, sigma, level, zfactor)
        std::vector refinements =
        {
            laser::Refinement { 0.00 * ms, 0.12 * mm, 2.4, 0.8 },
            laser::Refinement { 0.50 * ms, 0.12 * mm, 1.5 },
            laser::Refinement { 1.20 * ms, 0.16 * mm, 0.5 },
            //laser::Refinement { 4.00 * ms, 0.40 * mm, 2.5 },
            //laser::Refinement { 20.0 * ms, 0.46 * mm, 1.5 },
            //laser::Refinement { 60.0 * ms, 0.52 * mm, 0.5 },
        };

        auto initialCondition = spatial::constantFunction<D>( initialTemperature );

        // Initial discretization
        auto grid0 = makeRefinedGrid( nelements, lengths );
        auto grid1 = grid0;

        auto basis0 = makeHpBasis<AnsatzSpace>( grid0, grading );
        auto dofs0 = projectOnto( *basis0, initialCondition );

        // Postprocess initial condition
        std::vector postprocessors =
        {
            makeSolutionPostprocessor<D>( dofs0, 1 ),
            makeFunctionPostprocessor<D>( spatial::sliceLast( volumeSource, 0.0 ), "Source" ),
            makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, 0.0 )
        };

        auto filename = "outputs/timestepping_small_";

        postprocess( array::make<size_t, D>( 6 ), postprocessors, *basis0, filename + std::to_string( 0 ), 1 );

        size_t vtuInterval = 8;

        // Assembly stuff
        auto partitioner = NoPartitioner<D> { };
        auto solve = linalg::makeCGSolver( );

        // Integrate with with p + 1 integration points
        std::function orderDeterminor = [&]( CellIndex /* iElement */, std::array<size_t, D> degrees ) noexcept
        {
            return array::add<size_t>( degrees, 1 );
        };
 
        double dt = duration / nsteps;

        // Time stepping
        for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
        {
            double time0 = iTimeStep * dt;
            double time1 = ( iTimeStep + 1 ) * dt;

            // Create discetisation for t^{i+1}
            grid1 = makeRefinedGrid( nelements, lengths );
            grid1->refine( laser::makeRefinement<D>( laserTrack, refinements, time1 ) );
            
            auto basis1 = makeHpBasis<AnsatzSpace>( grid1, grading );

            std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
            std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;

            auto df = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis1 );

            std::vector<double> dofs1 = projectOnto( *basis0, *basis1, dofs0, df );
            std::vector<double> f( df.size1( ), 0.0 );

            double norm0 = 0.0;

            // Newton-Raphson iterations
            for( size_t i = 0; i < 40; ++i )
            {
                std::fill( f.begin( ), f.end( ), 0.0 );
                std::fill( df.data( ), df.data( ) + df.nnz( ), 0.0 );

                auto integrand = makeTimeSteppingThermalIntegrand<3>( material, phaseChange,
                    volumeSource, dofs0, dofs1, std::array{ time0, time1 }, theta );

                integrateOnDomain( *basis0, *basis1, integrand, { df, f }, partitioner, orderDeterminor );

                double norm1 = std::sqrt( std::inner_product( f.begin( ), f.end( ), f.begin( ), 0.0 ) );

                norm0 = i == 0 ? norm1 : norm0;

                std::cout << "iteration " << i + 1 << ": | F | = " << norm1 / norm0 << " (" << norm1 << ")" << std::endl;

                auto dx = solve( df, f );

                std::transform( dofs1.begin( ), dofs1.end( ), dx.begin( ), dofs1.begin( ), std::minus<double> { } );

                if(  norm1 / norm0 <= 1e-6 ) break;
            }

            // Postprocess vtu file with 6 sub-cells per element
            std::vector postprocessors =
            {
                makeSolutionPostprocessor<D>( dofs1, 1 ),
                makeFunctionPostprocessor<D>( spatial::sliceLast( volumeSource, time1 ), "Source" ),
                makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, time1 )
            };

            if( ( iTimeStep + 1 ) % vtuInterval == 0 )
            {
                postprocess( array::make<size_t, D>( 6 ), postprocessors, *basis1, 
                    filename + std::to_string( ( iTimeStep / vtuInterval ) + 1 ), 1 );
            }

            // Move discretization for next step
            dofs0 = std::move( dofs1 );
            grid0 = std::move( grid1 );
            basis0 = std::move( basis1 );
        }
    }
};

} // namespace mlhp

int main( )
{
    mlhp::Compute { }.compute( );
}


