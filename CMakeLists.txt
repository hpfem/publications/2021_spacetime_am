# This file is part of the 2021_spacetime_am project. License: See LICENSE

cmake_minimum_required( VERSION 3.12 )

project( spacetime_am LANGUAGES CXX )

# Configure mlhp: We need 4 dimensions, no python and as much performance as possible
set( MLHP_DIMENSIONS_TO_INSTANTIATE "1, 2, 3, 4" CACHE STRING "" )
set( MLHP_ENABLE_PYTHONBINDINGS OFF CACHE BOOL "" )
set( MLHP_DEBUG_CHECKS OFF CACHE BOOL "" )
set( MLHP_INDEX_SIZE_DOFS "64" CACHE STRING "" )

add_subdirectory( mlhp )

# Function for creating executable
function( createExample name )
    add_executable( ${name} ${name}.cpp )
    target_link_libraries( ${name} PRIVATE ${ARGN} )
    target_include_directories( ${name} PRIVATE . )
    set_target_properties( ${name} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )
endfunction( )

createExample( timestepping_AMB2018-02_coarse mlhp::core )
createExample( timestepping_AMB2018-02_fine mlhp::core )
createExample( timestepping_hatched_square mlhp::core )
createExample( timestepping_refinement_function mlhp::core )
createExample( timestepping_small mlhp::core )

find_package( MKL CONFIG QUIET COMPONENTS MKL_THREADING gnu_thread )

if( MKL_FOUND )

    add_library( spacetime INTERFACE )

    target_include_directories( spacetime INTERFACE $<TARGET_PROPERTY:MKL::MKL,INTERFACE_INCLUDE_DIRECTORIES> )
    target_compile_definitions( spacetime INTERFACE MKL_ILP64 ) # didn't work with INTERFACE_COMPILE_OPTIONS
    target_link_libraries( spacetime INTERFACE vtu11::vtu11 )

    if( UNIX )
        target_link_libraries( spacetime INTERFACE -L${MKL_ROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl )
        target_link_directories( spacetime INTERFACE ${MKL_ROOT}/lib/intel64 )
    else( UNIX )
        target_link_libraries( spacetime INTERFACE $<LINK_ONLY:MKL::MKL>)
    endif( UNIX )
 
    createExample( spacetime_AMB2018-02_coarse mlhp::core spacetime )
    createExample( spacetime_AMB2018-02_fine mlhp::core spacetime )
    createExample( spacetime_hatched_square mlhp::core spacetime  )
    createExample( spacetime_introduction_2D mlhp::core spacetime  )
    createExample( spacetime_zoom_for_tuning mlhp::core spacetime  )
   
else( MKL_FOUND )

    message( WARNING "MKL not found. Time-stepping drivers will still work." )

endif( MKL_FOUND )
