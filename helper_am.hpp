// This file is part of the 2021_spacetime_am project. License: See LICENSE

// Stuff related to the problem we are solving:
// * Heat source definition
// * Refinement strategies
// * Finite element integrands

#ifndef SPACETIME_AM_HELPER_AM_HPP
#define SPACETIME_AM_HELPER_AM_HPP

#include "mlhp/core.hpp"

namespace mlhp::laser
{

// Discrete point in laser path
template<size_t D>
struct Point
{
    std::array<double, D> xyz;
    double time;
    double intensity;
};

template<size_t D>
using LaserTrack = std::vector<Point<D>>;

// Discrete refinement point seen backwards from current time
struct Refinement
{
    double timeDelay;
    double sigma;
    double refinementLevel;
    double zfactor = 1.0;
};

template<size_t D>
auto makeTrack( const CoordinateList<D>& positions, double laserSpeed, double laserPower )
{
    MLHP_CHECK( positions.size( ) >= 2, "At least two laser positions required." );

    LaserTrack<D> track { Point<D>{ positions.front( ), 0.0, laserPower } };

    for( size_t iposition = 0; iposition < positions.size( ); ++iposition )
    {
        double dx = spatial::distance( track.back( ).xyz, positions[iposition] );

        track.push_back( Point<D>{ positions[iposition], track.back( ).time + dx / laserSpeed, laserPower } );
    }

    return track;
};

template<size_t D> inline
auto interpolateTrack( const LaserTrack<D>& track, double time )
{
    if( time == track.back( ).time )
    {
        return std::make_tuple( track.back( ).xyz, track.back( ).intensity );
    }

    auto predicate = [&]( const auto& point )
    {
        return point.time > time;
    };

    auto point1 = std::find_if( track.begin( ), track.end( ), predicate );
    auto point0 = point1 - 1;

    MLHP_CHECK( point1 != track.begin( ) && point1 != track.end( ), "LaserPoint not found ??" );

    double tau = ( time - point0->time ) / ( point1->time - point0->time );

    auto intensity = tau * ( point1->intensity - point0->intensity ) + point0->intensity;
    auto xyz = tau * ( point1->xyz - point0->xyz ) + point0->xyz;

    return std::make_tuple( xyz, intensity );
}

template<size_t D> inline
auto volumeSource( const LaserTrack<D>& track, double D4Sigma, double absorptivity, double depthScaling )
{
    double sigma = D4Sigma / 4.0;

    std::function shapeXY = spatial::integralNormalizedGaussBell<D - 1>( { }, sigma, absorptivity );
    std::function shapeZ = spatial::integralNormalizedGaussBell<1>( { }, sigma * depthScaling, 2.0 );

    std::function source = [=]( std::array<double, D + 1> xyzt )
    {
        auto [xyz, time] = array::peel( xyzt );

        if( time < track.front( ).time || time > track.back( ).time )
        {
            return 0.0;
        }

        auto [Pxyz, power] = interpolateTrack( track, time );
 
        auto [Pxy, Pz] = array::peel( Pxyz );
        auto [xy, z] = array::peel( xyz );

        return power * shapeXY( xy - Pxy ) * shapeZ( std::array{ z - Pz } );
    };

    return source;
}

template<size_t D> inline
auto evaluateRefinements( const std::vector<Refinement>& refinements,
                          double delay, std::array<double, D> difference )
{
    double eps = 1e-8 * ( refinements.back( ).timeDelay - refinements.front( ).timeDelay );

    for( size_t irefinement = 0; irefinement + 1 < refinements.size( ); ++irefinement )
    {
        auto& refinement0 = refinements[irefinement];
        auto& refinement1 = refinements[irefinement + 1];

        if( delay < refinement1.timeDelay - eps )
        {
            // Map into current refinement segment
            auto tau = utilities::mapToLocal0( refinement0.timeDelay, refinement1.timeDelay, delay );

            // Interpolate sigma and level in current refinement segment
            auto sigma = ( 1.0 - tau ) * refinement0.sigma + tau * refinement1.sigma;
            auto zfactor = ( 1.0 - tau ) * refinement0.zfactor + tau * refinement1.zfactor;
            auto maxlevel = ( 1.0 - tau ) * refinement0.refinementLevel + tau * refinement1.refinementLevel;

            difference.back( ) /= zfactor;

            auto distance = spatial::norm( difference );

            // Evaluate exponential
            auto level = maxlevel * std::exp( -distance * distance / ( 2.0 * sigma * sigma ) );

            return static_cast<RefinementLevel>( std::round( level ) );
        }
    }

    return RefinementLevel { 0 };
}

template<size_t D> inline
auto refinementLevelBasedOnLaserHistory( const LaserTrack<D>& track,
                                         const std::vector<Refinement>& refinements )
{
    auto delay0 = refinements.front( ).timeDelay;
    auto delay1 = refinements.back( ).timeDelay;

    return [=]( std::array<double, D + 1> xyzt )
    {
        auto [xyz, time] = array::peel( xyzt );

        RefinementLevel level = 0;

        // Loop over laser path segments
        for( size_t iSegment = 0; iSegment + 1 < track.size( ); ++iSegment )
        {
            auto& point0 = track[iSegment];
            auto& point1 = track[iSegment + 1];

            // Continue if segment is not yet active, or finished longer ago than delay
            if( point0.time <= time + delay0 && point1.time >= time - delay1 )
            {
                // If second point of segment is in future, then interpolate with current time
                auto alpha = ( time + delay0 - point0.time ) / ( point1.time - point0.time );

                alpha = std::clamp( alpha, 0.0, 1.0 );

                auto xyz1 = ( 1.0 - alpha ) * point0.xyz + alpha * point1.xyz;

                auto [p, t] = spatial::closestPointOnSegment( point0.xyz, xyz1, xyz );

                t *= alpha;

                // Local coordinate of projection p along axis point0 - point1
                double tau = time - ( 1.0 - t ) * point0.time - t * point1.time;

                level = std::max( level, evaluateRefinements( refinements, tau, p - xyz ) );
            }
        }

        return level;
    };
}

// Refinement based on laser history sliced for given time (for time-stepping)
template<size_t D> inline
auto makeRefinement( const LaserTrack<D>& track,
                     const std::vector<Refinement>& refinements,
                     double time )
{
    auto levelFunction = refinementLevelBasedOnLaserHistory( track, refinements );

    auto slicedLevelFunction = [=]( std::array<double, D> xyz )
    {
        return levelFunction( array::insert( xyz, D, time ) );
    };

    return refineWithLevelFunction<D>( slicedLevelFunction, 7 );
}

// Refinement based on laser history as D + 1 dimensional function (for space-time)
template<size_t D> inline
auto makeRefinement( const LaserTrack<D>& track,
                     const std::vector<Refinement>& refinements )
{
    auto levelFunction = refinementLevelBasedOnLaserHistory( track, refinements );

    return refineWithLevelFunction<D + 1>( levelFunction, 5 );
}

// Postprocess for time slice (for time-stepping)
template<size_t D>
auto makeRefinementLevelFunctionPostprocessor( const LaserTrack<D>& track,
                                               const std::vector<Refinement>& refinements,
                                               double time )
{
    auto depthInt = refinementLevelBasedOnLaserHistory<D>( track, refinements );

    auto slicedDepth = [=]( std::array<double, D> xyz )
    {
        return static_cast<double>( depthInt( array::insert( xyz, D, time ) ) );
    };

    return makeFunctionPostprocessor<D>( std::function { slicedDepth }, "RefinementLevel" );

}

// Postprocess volumetrically (for space-time, will be sliced later)
template<size_t D>
auto makeRefinementLevelFunctionPostprocessor( const LaserTrack<D>& track,
                                               const std::vector<Refinement>& refinements )
{
    auto depthInt = refinementLevelBasedOnLaserHistory<D>( track, refinements );

    auto slicedDouble = [=]( std::array<double, D + 1> xyzt ) { return static_cast<double>( depthInt( xyzt ) ); };

    return makeFunctionPostprocessor<D + 1>( std::function { slicedDouble }, "RefinementLevel" );
}

auto hatchedSquarePositions( std::array<double, 3> center, double width, double trackDistance )

{
    static constexpr size_t D = 3;

    CoordinateList<D> laserPositions;

    auto append = [&]( double x, double y )
    {
        laserPositions.push_back( { x, y, center[2] } );
    };

    // Frame
    append( center[0] + width / 2.0, center[1] - width / 2.0 );
    append( center[0] + width / 2.0, center[1] + width / 2.0 );
    append( center[0] - width / 2.0, center[1] + width / 2.0 );
    append( center[0] - width / 2.0, center[1] - width / 2.0 );
    append( center[0] + width / 2.0, center[1] - width / 2.0 );

    // Interior
    double xmin = center[0] - width / 2.0 + trackDistance;
    double ymin = center[1] - width / 2.0 + trackDistance;
    double xmax = center[0] + width / 2.0 - trackDistance;
    double ymax = center[1] + width / 2.0 - trackDistance;

    auto ntracks = std::lround( 2.0 * ( width - 2.0 * trackDistance ) / ( std::sqrt( 2.0 ) * trackDistance ) );
    auto increment = 2.0 * ( width - 2.0 * trackDistance ) / ntracks;

    MLHP_CHECK( ntracks > 2, "No inside tracks." );

    // First point bottom right
    append( xmax, ymin );

    // Tracks
    for( int itrack = 1; itrack < ntracks; ++itrack )
    {
        auto t = itrack * increment;

        // Bottom left and top right points
        append( std::max( xmax - t, xmin ), std::max( ymin - ( ymax - ymin ) + t, ymin ) );
        append( std::min( xmax + ( xmax - xmin ) - t, xmax ), std::min( ymin + t, ymax ) );

        if( itrack % 2 != 0 )
        {
            std::iter_swap( laserPositions.end( ) - 1, laserPositions.end( ) - 2 );
        }
    }

    // Last point top left
    append( xmin, ymax );

    return laserPositions;
}

} // namespace mlhp::laser

namespace mlhp
{

struct Units
{
    double m = 100.0;
    double cm = 1.0;
    double mm = 1e-3 * m;
    double um = 1e-6 * m;
    double s = 1.0;
    double ms = 1e-3 * s;
    double kg = 1000.0;
    double g = 1e-3 * kg;
    double J = 1.0, W = 1.0, C = 1.0;
};

// Give polynomial degrees separately for space and time for each refinement level
template<size_t D, std::integral Integer> inline
auto perLevelGrading( std::initializer_list<Integer> spatialDegrees,
                      std::initializer_list<Integer> temporalDegrees )
{
    return PerLevelGrading { [=]( RefinementLevel level, RefinementLevel maxLevel )
    {
        MLHP_CHECK( spatialDegrees.size( ) >= maxLevel + 1, "Fewer degrees than refinement levels." );
        MLHP_CHECK( temporalDegrees.size( ) >= maxLevel + 1, "Fewer degrees than refinement levels." );

        size_t ps = std::data( spatialDegrees )[level];
        size_t pt = std::data( temporalDegrees )[level];

        return PolynomialDegreeTuple { array::insert( array::make<size_t, D>( ps ), D, pt ) };
    } };
}

// Returns [fpc, dfpc/dT, ddfpc/ddT]
auto regularizedStepFunction( double Ts, double Tl, double T, double S )
{
    double Tmid = ( Tl + Ts ) / 2.0;
    double Tstd = ( Tl - Ts ) / 2.0 * S;
    double xi = ( T - Tmid ) / Tstd;

    if( std::abs( xi ) < 5.0 )
    {
        auto tanhXi = std::tanh( xi );

        double fpc_d0 = 0.5 * ( tanhXi + 1.0 );
        double fpc_d1 = ( 1.0 - tanhXi * tanhXi ) / ( 2.0 * Tstd );
        double fpc_d2 = ( tanhXi * tanhXi - 1.0 ) * tanhXi / ( Tstd * Tstd );

        return std::array { fpc_d0, fpc_d1, fpc_d2 };
    }
    else
    {
        return std::array { xi > 0.0 ? 1.0 : 0.0, 0.0, 0.0 };
    }
}

struct NonlinearHeatParameters
{
    double specificCapacity;
    double specificCapacityPrime;
    double conductivity;
    double conductivityPrime;
};

// [capacity, capacity prime, conductivity, conductivity prime]( temperature )
using NonlinearHeatMaterial = std::function<NonlinearHeatParameters( double T )>;

struct PhaseChangeParameters
{
    double density;            // Constant density (around phase change)
    double latentHeat;         // Energy in phase change
    double liquidConductivity; // Conductivity in liquid phase
    double Ts;                 // Tmin of melt interval
    double Tl;                 // Tmax of melt interval
    double S;                  // Smoothness of phase change
};

template<size_t D>
auto makeTimeSteppingThermalIntegrand( const NonlinearHeatMaterial& material,
                                       PhaseChangeParameters phaseChange,
                                       const spatial::ScalarFunction<D + 1>& source,
                                       const std::vector<double>& dofs0,
                                       const std::vector<double>& dofs1,
                                       std::array<double, 2> timeStep,
                                       double theta = 1.0 )
{
    auto evaluate = [=, &dofs0, &dofs1]( const LocationMap& locationMap0,
                                         const LocationMap& locationMap1,
                                         const BasisFunctionEvaluation<D>& shapes0,
                                         const BasisFunctionEvaluation<D>& shapes1,
                                         AlignedDoubleVectors& targets,
                                         double weightDetJ )
    {
        auto ndof = shapes1.ndof( );
        auto nblocks = shapes1.nblocks( );
        auto ndofpadded = shapes1.ndofpadded( );

        auto N = shapes1.noalias( 0, 0 );
        auto dN = shapes1.noalias( 0, 1 );

        auto u0 = evaluateSolution( shapes0, locationMap0, dofs0 );
        auto u1 = evaluateSolution( shapes1, locationMap1, dofs1 );
        auto du0 = evaluateGradient( shapes0, locationMap0, dofs0 );
        auto du1 = evaluateGradient( shapes1, locationMap1, dofs1 );

        auto [c0, cPrime0, k0, kPrime0] = material( u0 );
        auto [c1, cPrime1, k1, kPrime1] = material( u1 );

        double dt = timeStep[1] - timeStep[0];
        double L = phaseChange.latentHeat * phaseChange.density;

        auto [fpc0, dfpc0, ddfpc0] = regularizedStepFunction( phaseChange.Ts, phaseChange.Tl, u0, phaseChange.S );
        auto [fpc1, dfpc1, ddfpc1] = regularizedStepFunction( phaseChange.Ts, phaseChange.Tl, u1, phaseChange.S );

        k0 += fpc0 * phaseChange.liquidConductivity;
        k1 += fpc1 * phaseChange.liquidConductivity;

        double cTheta = ( 1.0 - theta ) * c0 + theta * c1;
        double mass = ( cTheta + theta * cPrime1 * ( u1 - u0 ) + L * dfpc1 ) / dt;

        double factor = theta * k1;

        linalg::symmetricElementLhs( targets[0].data( ), ndof, nblocks, [&]( size_t i, size_t j )
        {
            double value = N[i] * N[j] * mass;

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += dN[axis * ndofpadded + i] * dN[axis * ndofpadded + j] * factor;

            } // component

            return value * weightDetJ;
        } );

        double source1 = theta == 0.0 ? 0.0 : source( array::insert( shapes1.xyz( ), D, timeStep[1] ) );
        double source0 = theta == 1.0 ? 0.0 : source( array::insert( shapes1.xyz( ), D, timeStep[0] ) );

        double sourceTheta = ( 1.0 - theta ) * source0 + theta * source1;

        double dc = cTheta * ( u1 - u0 ) / dt;
        double dL = L * ( fpc1 - fpc0 ) / dt;

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&]( size_t i )
        {
            double value = N[i] * ( dc + dL - sourceTheta );

            for( size_t axis = 0; axis < D; ++axis )
            {
                double kuTheta = ( 1.0 - theta ) * k0 * du0[axis] + theta * k1 * du1[axis];

                value += dN[axis * ndofpadded + i] * kuTheta;
            }

            return value * weightDetJ;
        } );
    };

    auto types = std::vector { AssemblyType::SymmetricMatrix, AssemblyType::Vector };

    return BasisProjectionIntegrand<D>( types, DiffOrders::FirstDerivatives, evaluate );
}

auto makePhaseChangeMaterial( const NonlinearHeatMaterial& material,
                              const PhaseChangeParameters& phaseChange )
{
    return NonlinearHeatMaterial { [=]( double T )
    {
        auto [c, cPrime, k, kPrime] = material( T );
        auto [rho, L, K, Ts, Tl, S] = phaseChange;
        auto [fpc, dfpc, ddfpc] = regularizedStepFunction( Ts, Tl, T, S );

        c += L * dfpc * rho;
        k += K * fpc;
        cPrime += L * ddfpc * rho;
        kPrime += K * dfpc;

        return NonlinearHeatParameters { c, cPrime, k, kPrime };
    } };
}

template<size_t D>
const auto& createTestFilter(const AbsBasis<D + 1>& basis, const BasisFunctionEvaluation<D + 1>& trialShapes, bool petrov)
{
    thread_local struct 
    {
        const AbsBasis<D + 1>* basis;
        CellIndex ielement;
        std::vector<TensorProductIndices<D + 1>> indices;
        std::vector<double> filter;
    } test;

     
    // Setup shape function container if basis or element index are new
    if(test.basis != &basis || test.ielement != trialShapes.elementIndex( ))
    {
        test.basis = &basis;
        test.filter.resize(trialShapes.ndofpadded( ), 0.0);

        if(petrov)
        {
            auto& elementFilterBasis = dynamic_cast<const ElementFilterBasis<D + 1>&>(basis);
            auto& mlhpBasis = dynamic_cast<const MultilevelHpBasis<D + 1>&>(elementFilterBasis.unfilteredBasis( ));
            auto& hierarchicalGrid = mlhpBasis.hierarchicalGrid( );
            auto fullIndex = hierarchicalGrid.fullIndex(trialShapes.elementIndex( ));

            size_t dofIndex = 0;
            for(auto index = fullIndex; index != NoCell; index = hierarchicalGrid.parent(index))
            {
                mlhpBasis.tensorProductIndices(index, 0, test.indices);

                for(auto ijk : test.indices)
                {
                    test.filter[dofIndex++] = ijk[D] != 0 ? 1.0 : 0.0;
                }
            }
        }
        else
        {
            std::fill(test.filter.begin( ), test.filter.end( ), 1.0);
        }
    }
  
    return test.filter;
}

template<size_t D>
auto testFunctions( const BasisFunctionEvaluation<D + 1>& shapes, bool petrov )
{
    if( petrov )
    {
        // Return pointer to time derivatives (row D in shape function derivatives)
        return shapes.noalias( 0, 1 ) + D * shapes.ndofpadded( );
    }
    else
    {
        // Return pointer to shape functions (without derivative)
        return shapes.noalias( 0, 0 );
    }
}

template<size_t D>
auto testGradient( const BasisFunctionEvaluation<D + 1>& shapes, bool petrov )
{
    auto ndofpadded = shapes.ndofpadded( );

    std::array<const double* MLHP_RESTRICT, D> ptrs;

    if( petrov )
    {
        // Construct pointers to mixed derivative terms [d2N/(dt*dx), d2N/(dt*dy), d2N/(dt*dz)]
        auto ptr = shapes.noalias( 0, 2 ) + D * ndofpadded;

        for( size_t axis = 0; axis < D; ++axis )
        {
            ptrs[axis] = memory::assumeAlignedNoalias( ptr );

            ptr += ( D - axis ) * ndofpadded;
        }
    }
    else
    {
        // Simply assign pointers to shape function gradient
        for( size_t axis = 0; axis < D; ++axis )
        {
            ptrs[axis] = shapes.noalias( 0, 1 ) + axis * ndofpadded;
        }
    }

    return ptrs;
}

template<size_t D>
auto makeSpaceTimeThermalIntegrand( const NonlinearHeatMaterial& material,
                                    const spatial::ScalarFunction<D + 1>& source,
                                    const std::vector<double>& dofs, bool lhs, 
                                    const AbsBasis<D + 1>& basis )
{
    bool petrov = true;

    auto evaluate = [=, &basis]( const BasisFunctionEvaluation<D + 1>& shapes,
                                 const LocationMap& locationMap,
                                 AlignedDoubleVectors& targets,
                                 AlignedDoubleVector&,
                                 double weightDetJ )
    { 
        auto ndof = shapes.ndof( );
        auto nblocks = shapes.nblocks( );
        auto ndofpadded = shapes.ndofpadded( );

        auto u = evaluateSolution( shapes, locationMap, dofs );
        auto du = evaluateGradient( shapes, locationMap, dofs );

        double f = source( shapes.xyz( ) );

        auto [c, cPrime, k, kPrime] = material( u );

        auto& testfilter = createTestFilter<D>( basis, shapes, petrov );      

        auto trialN = shapes.noalias( 0, 0 );
        auto trialDN = shapes.noalias( 0, 1 );

        auto testN = testFunctions<D>( shapes, petrov );
        auto testDN = testGradient<D>( shapes, petrov );

        auto rhs = lhs ? targets[1].data( ) : targets[0].data( );

        linalg::elementRhs( rhs, ndof, nblocks, [&]( size_t i )
        { 
            double value = testN[i] * ( c * du[D] - f );

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += testDN[axis][i] * k * du[axis];
            }

            return value * weightDetJ * testfilter[i];
        } );     

        // We evaluate only the residual
        if( !lhs ) return; 

        linalg::unsymmetricElementLhs( targets[0].data( ), ndof, nblocks, [=]( size_t i, size_t j )
        { 
            double dNjdt = trialDN[D * ndofpadded + j];
            double value = testN[i] * ( c * dNjdt + cPrime * du[D] * trialN[j] );

            for( size_t axis = 0; axis < D; ++axis )
            {
                double dNi = testDN[axis][i];
                double dNj = trialDN[axis * ndofpadded + j];

                value += dNi * ( du[axis] * kPrime * trialN[j] + k * dNj );

            } // component

            return value * weightDetJ * testfilter[i];
        } );
    };

    auto diffOrder = petrov ? DiffOrders::SecondDerivatives : DiffOrders::FirstDerivatives;

    auto types = lhs ? std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector } :
                       std::vector { AssemblyType::Vector };

    return DomainIntegrand<D + 1>( diffOrder, types, evaluate );
}

template<size_t D>
auto phaseChangeIntegrandFactory( NonlinearHeatMaterial material,
                                  PhaseChangeParameters phaseChange,
                                  const spatial::ScalarFunction<D + 1>& source )
{
    return [=](const AbsBasis<D + 1>& basis, const std::vector<double>& dofs, size_t iteration, bool computeJacobian)
    { 
        if( computeJacobian && iteration == 0 )
        {
            return makeSpaceTimeThermalIntegrand<D>( material, source, dofs, computeJacobian, basis );
        }
        else
        {
            auto model = phaseChange;

            model.S = iteration < 2 ? std::max( 10.0, model.S ) : model.S;

            auto phaseChangeMaterial = makePhaseChangeMaterial( material, model );

            return makeSpaceTimeThermalIntegrand<D>( phaseChangeMaterial, source, dofs, computeJacobian, basis );
        }
    };
}

template<size_t D>
void printTrack( const laser::LaserTrack<D>& track )
{
    std::cout << "import numpy" << std::endl;
    std::cout << "import matplotlib.pyplot as plt" << std::endl;
    std::cout << "data = numpy.array([";

    for( auto [xyz, time, intensity] : track )
    {
        std::cout << "[";
        for( auto value : xyz )
        {
            std::cout << value << ", ";
        }

        std::cout << time << ", " << intensity << "]," << std::endl;
    }
    std::cout << "])" << std::endl;

    std::cout << "x, y = data[:, 0], data[:, 1]" << std::endl;
    std::cout << "time, intensity = data[:, " << D << "], data[:, " << D + 1 << "]" << std::endl;

    std::cout << "plt.plot(x, y)" << std::endl;
    std::cout << "plt.plot([0.0, 2.4, 2.4, 0.0, 0.0], [0.0, 0.0, 2.4, 2.4, 0.0], 'black' )" << std::endl;
    std::cout << "plt.axis('equal')" << std::endl;
    std::cout << "plt.show()" << std::endl;
}

} // namespace mlhp

#endif // SPACETIME_AM_HELPER_AM_HPP
